package gui;

import model.Lists;
import model.Veiculo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by Christian on 19/05/15.
 */
public class LsVeiculoScreen extends JDialog {
    private JDialog janela = this;
    private JPanel rootPane;
    private JTable tableVeiculos;

    public LsVeiculoScreen(JFrame owner){
        super(owner, "Lista de veículos", true);
        setContentPane(rootPane);
        addTable();
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    private void addTable(){
        DefaultTableModel defaultTableModel = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        tableVeiculos.setModel(defaultTableModel);
        defaultTableModel.addColumn("Modelo");
        defaultTableModel.addColumn("Ano");
        defaultTableModel.addColumn("Diária Padrão");

        Lists lists = Lists.getInstance();
        ArrayList<Veiculo> veiculos = lists.getListVeiculo();

        for(Veiculo v : veiculos){
            defaultTableModel.addRow(new Object[]{v.getModelo(), v.getAno(), "R$ " + Double.toString(v.getDiaria())});
        }
        tableVeiculos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2){
                    int selRow = tableVeiculos.getSelectedRow();
                    if(selRow > -1){
                        String modelo = tableVeiculos.getModel().getValueAt(selRow, 0).toString();
                        String ano = tableVeiculos.getModel().getValueAt(selRow, 1).toString();
                        String diaria = tableVeiculos.getModel().getValueAt(selRow, 2).toString();
                        VeiculoScreen veiculoScreen = new VeiculoScreen(janela, modelo, ano, diaria);
                    }
                }
            }
        });
    }
}

package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by Christian on 13/05/15.
 */
public class TelaInicial extends JFrame{
    private JPanel rootPane;
    private JFrame janela = this;

    public TelaInicial() {
        super("Locadora de Veículos");
        setContentPane(rootPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        createMenuBar();
        pack();
        setSize(1080, 720);
        setLocationRelativeTo(null);
        setVisible(true);
    } 
    private void createMenuBar(){
        /**********************************************
         * Menu Bar                                   *
         **********************************************/
        JMenuBar menuBar = new JMenuBar();

        /**********************************************
         * Menus                                      *
         **********************************************/
        JMenu cadastrosMenu = new JMenu("Cadastros");
        cadastrosMenu.setMnemonic(KeyEvent.VK_C);
        menuBar.add(cadastrosMenu);
        JMenu listasMenu = new JMenu("Listas");
        listasMenu.setMnemonic(KeyEvent.VK_L);
        menuBar.add(listasMenu);

        /**********************************************
         * Menu Itens                                 *
         **********************************************/
        JMenuItem cadastroVeiculo = new JMenuItem("Cadastro de Veículo");
        cadastrosMenu.add(cadastroVeiculo);
        JMenuItem listaVeiculos = new JMenuItem("Lista Veiculos");
        listasMenu.add(listaVeiculos);
        JMenuItem cadastroPromocao = new JMenuItem("Cadastrar Promoção");
        cadastrosMenu.add(cadastroPromocao);
        JMenuItem listaPromocoes = new JMenuItem("Lista Promoçoes");
        listasMenu.add(listaPromocoes);
        JMenuItem listaInteressados = new JMenuItem("Lista de Interesse");
        listasMenu.add(listaInteressados);
        JMenuItem listaLocacoes = new JMenuItem("Lista de Locações");
        listasMenu.add(listaLocacoes);

        /**********************************************
         * Menu Itens Action Listeners                *
         **********************************************/
        cadastroVeiculo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog cdVeiculoDialog = new CdVeiculoScreen(janela);
            }
        });
        listaVeiculos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog lsVeiculoDialog = new LsVeiculoScreen(janela);
            }
        });
        listaPromocoes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog lsPromoDialog = new LsPromocoesScreen(janela);
            }
        });
        cadastroPromocao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog cdPromoDialog = new CdPromocaoScreen(janela);
            }
        });
        listaInteressados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog lsInteressadosDialog = new LsInteresseScreen(janela);
            }
        });
        listaLocacoes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog lsLocacoesDiealog = new LsLocacoesScreen(janela);
            }
        });

        this.setJMenuBar(menuBar);
    }
}

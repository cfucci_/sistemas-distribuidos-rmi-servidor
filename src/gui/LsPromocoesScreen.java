package gui;

import model.Lists;
import model.Promocao;
import model.Veiculo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by Christian on 22/05/15.
 */
public class LsPromocoesScreen extends JDialog{
    private JTable tablePromos;
    private JPanel rootPane;
    private Veiculo veiculo;

    public LsPromocoesScreen(JFrame owner){
        super(owner, "Lista de veículos", true);
        setContentPane(rootPane);
        addTable("", "");
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }
    public LsPromocoesScreen(JDialog owner, String modelo, String ano){
        super(owner, "Lista de veículos", true);
        setContentPane(rootPane);
        addTable(modelo, ano);
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    private void addTable(String modelo, String ano) {
        DefaultTableModel model = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        tablePromos.setModel(model);
        model.addColumn("Modelo");
        model.addColumn("Ano");
        model.addColumn("Diária Promocional");
        model.addColumn("Data de início");
        model.addColumn("Data de fim");

        Lists lists = Lists.getInstance();
        ArrayList<Promocao> promos = lists.getListPromocoes();
        if(!modelo.equals("") && !ano.equals("")){
            for(Promocao p : promos){
                if (p.getVeiculo().getModelo().equals(modelo) && Integer.toString(p.getVeiculo().getAno()).equals(ano)){
                    model.addRow(new Object[]{p.getVeiculo().getModelo(), p.getVeiculo().getAno(), "R$"+Double.toString(p.getDiaria()), p.getDataInicio(), p.getDataFim()});
                }
            }
        }else{
            for(Promocao p : promos){
                model.addRow(new Object[]{p.getVeiculo().getModelo(), p.getVeiculo().getAno(), "R$"+Double.toString(p.getDiaria()), p.getDataInicio(), p.getDataFim()});
            }
        }

    }
}

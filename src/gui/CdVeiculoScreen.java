package gui;

import controller.VeiculoController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Christian on 18/05/15.
 */
public class CdVeiculoScreen extends JDialog{
    private JPanel rootPane;
    private JTextField modeloTextField;
    private JTextField anoTextField;
    private JTextField diariaTextField;
    private JButton salvarButton;

    public CdVeiculoScreen(JFrame owner){
        super(owner,"Cadastro de veículos", true);
        setContentPane(rootPane);
        setResizable(false);
        pack();
        setLocationRelativeTo(owner);
        setButtons();
        setVisible(true);
    }

    public void setButtons(){
        salvarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String modelo;
                int ano = 0;
                double diaria = 0.0;
                modelo = modeloTextField.getText();
                try{
                    ano = Integer.parseInt(anoTextField.getText());
                    diaria = Double.parseDouble(diariaTextField.getText());
                    VeiculoController veiculoController = new VeiculoController();
                    veiculoController.createVeiculo(modelo, ano, diaria);
                    JOptionPane.showMessageDialog(getParent(), "Sucesso", "Veículo Cadastrado!", JOptionPane.INFORMATION_MESSAGE);
                    exitAction(e);
                } catch (NumberFormatException exc){
                    JOptionPane.showMessageDialog(getParent(),"Erro nas entradas", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
    }

    private void exitAction(ActionEvent event){
        this.dispose();
    }
}

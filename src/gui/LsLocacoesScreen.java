package gui;

import model.Lists;
import model.Locacao;
import model.Veiculo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by Christian on 27/05/15.
 */
public class LsLocacoesScreen extends JDialog{
    private JPanel rootPane;
    private JTable locTable;

    public LsLocacoesScreen(JFrame owner){
        super(owner, "Lista de veículos", true);
        setContentPane(rootPane);
        addTable();
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    private void addTable() {
        DefaultTableModel defaultTableModel = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        locTable.setModel(defaultTableModel);
        defaultTableModel.addColumn("Modelo");
        defaultTableModel.addColumn("Ano");
        defaultTableModel.addColumn("Diária");
        defaultTableModel.addColumn("Local Ret.");
        defaultTableModel.addColumn("Local Dev.");
        defaultTableModel.addColumn("Data Ret.");
        defaultTableModel.addColumn("Data Dev.");
        defaultTableModel.addColumn("Idade Cond.");
        defaultTableModel.addColumn("Num. Parcelas");

        Lists lists = Lists.getInstance();
        ArrayList<Veiculo> veiculos = lists.getListVeiculo();
        for(Veiculo v : veiculos){
            ArrayList<Locacao> locs = v.getLocacoes();
            for(Locacao l : locs){
                defaultTableModel.addRow(new Object[]{v.getModelo(), Integer.toString(v.getAno()), Double.toString(v.getDiaria()),
                                         l.getLocalRetirada(), l.getLocalDevolucao(), l.getDataHoraRetirada().toString(), l.getDataHoraDevolucao().toString(),
                                         Integer.toString(l.getIdadeConcutor()), Integer.toString(l.getParcelas())});
            }
        }
    }
}

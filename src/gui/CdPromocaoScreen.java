package gui;

import controller.PromocaoController;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by Christian on 22/05/15.
 */
public class CdPromocaoScreen extends JDialog{
    private JPanel rootPane;
    private JTextField modeloTextField;
    private JTextField anoTextField;
    private JFormattedTextField diariaTextField;
    private JFormattedTextField dataIniTextField;
    private JFormattedTextField dataFimTextField;
    private JButton salvarButton;
    private boolean fromTable = false;

    public CdPromocaoScreen(JFrame owner){
        super(owner, "Cadastro de Promoções", true);
        setFormatters();
        setContentPane(rootPane);
        pack();
        setLocationRelativeTo(owner);
        fromTable = false;
        setButtons();
        setVisible(true);
    }

    public CdPromocaoScreen(JDialog owner, String modelo, String ano){
        super(owner, "Cadastro de Promoções", true);
        setFormatters();
        setContentPane(rootPane);
        pack();
        setLocationRelativeTo(owner);
        fromTable = true;
        modeloTextField.setText(modelo);
        anoTextField.setText(ano);
        setButtons();
        setVisible(true);
    }

    private void setFormatters() {
        try {
            MaskFormatter maskData = new MaskFormatter("##/##/#### ##:##");
            maskData.setPlaceholderCharacter('_');
            DefaultFormatterFactory defaultFormatterFactory = new DefaultFormatterFactory(maskData);
            dataIniTextField.setFormatterFactory(defaultFormatterFactory);
            dataFimTextField.setFormatterFactory(defaultFormatterFactory);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setButtons() {
        salvarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PromocaoController promocaoController = new PromocaoController();
                promocaoController.createPromocao(modeloTextField.getText(),anoTextField.getText(),diariaTextField.getText(),dataIniTextField.getText(),dataFimTextField.getText());
                JOptionPane.showMessageDialog(getParent(),"Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
    }
}

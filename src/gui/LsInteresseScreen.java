package gui;

import model.Interesse;
import model.Lists;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by Christian on 27/05/15.
 */
public class LsInteresseScreen extends JDialog {
    private JPanel rootPane;
    private JTable tableInteresse;

    public LsInteresseScreen(JFrame owner){
        super(owner, "Lista de veículos", true);
        setContentPane(rootPane);
        addTable();
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    private void addTable() {
        DefaultTableModel defaultTableModel = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        tableInteresse.setModel(defaultTableModel);
        defaultTableModel.addColumn("Modelo");
        defaultTableModel.addColumn("Ano");
        defaultTableModel.addColumn("Diária Padrão");
        defaultTableModel.addColumn("Data Retirada");
        defaultTableModel.addColumn("Data Devolução");

        Lists list = Lists.getInstance();
        ArrayList<Interesse> registrados = list.getListInteresse();

        for(Interesse i : registrados){
            defaultTableModel.addRow(new Object[]{i.getVeiculo().getModelo(), Integer.toString(i.getVeiculo().getAno()), Double.toString(i.getVeiculo().getDiaria()), i.getDataHoraRetirada().toString(), i.getDataHoraDevolucao().toString()});
        }
    }
}

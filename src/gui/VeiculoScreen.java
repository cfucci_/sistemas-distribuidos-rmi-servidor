package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Christian on 22/05/15.
 */
public class VeiculoScreen extends JDialog {
    private JDialog janela = this;
    private JPanel rootPane;
    private JButton verPromoçoesButton;
    private JButton cadastrarPromoçãoButton;
    private JLabel modeloText;
    private JLabel anoText;
    private JLabel diariaText;

    public VeiculoScreen(JDialog owner, String modelo, String ano, String diaria){
        super(owner, "Veículo", true);
        modeloText.setText(modelo);
        anoText.setText(ano);
        diariaText.setText(diaria);
        setButtons();
        setContentPane(rootPane);
        pack();
        setLocationRelativeTo(owner);
        setVisible(true);
    }

    public void setButtons(){
        cadastrarPromoçãoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog cdPromo = new CdPromocaoScreen(janela, modeloText.getText(), anoText.getText());
            }
        });
        verPromoçoesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog lsPromos = new LsPromocoesScreen(janela, modeloText.getText(), anoText.getText());
            }
        });
    }
}

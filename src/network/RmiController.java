package network;

import controller.InteresseControllerRemote;
import controller.VeiculoController;
import controller.VeiculoControllerRemote;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Christian on 25/05/15.
 */
public class RmiController {
    private static RmiController instance = new RmiController();

    protected RmiController(){
        System.setProperty("java.security.policy", "file:/C:/Users/Christian/IdeaProjects/Distribuidos JAVA RMI/security.policy");
        System.setSecurityManager(new RMISecurityManager());
        System.out.println("Ready!");
    }

    public static RmiController getInstance(){
        return instance;
    }
    public void createRemoteObjectVeiculo(VeiculoControllerRemote o){
        try {
            Naming.rebind("rmi://localhost/veiculo", o);
        } catch (RemoteException e) {
            System.out.println("Servidor falhou: " + e.getMessage());
        } catch (MalformedURLException e) {
            System.out.println("URL falou: " + e.getMessage());
        }
    }
    public void createRemoteObjectInteresse(InteresseControllerRemote o){
        try {
            Naming.rebind("rmi://localhost/interesse", o);
        } catch (RemoteException e) {
            System.out.println("Servidor falhou: " + e.getMessage());
        } catch (MalformedURLException e) {
            System.out.println("URL falou: " + e.getMessage());
        }
    }
}

package model;

import java.util.ArrayList;

/**
 * Created by Christian on 10/05/15.
 */
public class Lists {
    private ArrayList<Veiculo> listVeiculo = new ArrayList<Veiculo>();
    private ArrayList<Interesse> listInteresse = new ArrayList<Interesse>();
    private ArrayList<Promocao> listPromocoes = new ArrayList<Promocao>();
    private static Lists instance = new Lists();

    protected void VeiculoList(){
        //protege a instancia
    }

    public static Lists getInstance(){
        return instance;
    }

    public ArrayList<Veiculo> getListVeiculo(){
        return listVeiculo;
    }

    public ArrayList<Interesse> getListInteresse() {
        return listInteresse;
    }

    public ArrayList<Promocao> getListPromocoes() {
        return listPromocoes;
    }
}

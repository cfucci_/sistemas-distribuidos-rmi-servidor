package model;

import org.joda.time.DateTime;

import java.net.InetAddress;
import java.util.Date;

/**
 * Created by Christian on 12/05/15.
 */
public class Interesse {
    private Veiculo veiculo;
    private String endCliente;
    private DateTime dataHoraRetirada;
    private DateTime dataHoraDevolucao;

    public Interesse(Veiculo veiculo, String endCliente, DateTime dataHoraRetirada, DateTime dataHoraDevolucao) {
        this.veiculo = veiculo;
        this.endCliente = endCliente;
        this.dataHoraRetirada = dataHoraRetirada;
        this.dataHoraDevolucao = dataHoraDevolucao;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public String getEndCliente() {
        return endCliente;
    }

    public DateTime getDataHoraRetirada() {
        return dataHoraRetirada;
    }

    public DateTime getDataHoraDevolucao() {
        return dataHoraDevolucao;
    }
}

package model;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Christian on 12/05/15.
 */
public class Locacao implements Serializable {
    private String localRetirada;
    private String localDevolucao;
    private DateTime dataHoraRetirada;
    private DateTime dataHoraDevolucao;
    private int idadeConcutor;
    private long numCC;
    private int parcelas;

    public Locacao(String localRetirada, String localDevolucao, DateTime dataHoraRetirada, DateTime dataHoraDevolucao, int idadeConcutor, long numCC, int parcelas) {
        this.localRetirada = localRetirada;
        this.localDevolucao = localDevolucao;
        this.dataHoraRetirada = dataHoraRetirada;
        this.dataHoraDevolucao = dataHoraDevolucao;
        this.idadeConcutor = idadeConcutor;
        this.numCC = numCC;
        this.parcelas = parcelas;
    }

    public String getLocalRetirada() {
        return localRetirada;
    }

    public String getLocalDevolucao() {
        return localDevolucao;
    }

    public DateTime getDataHoraRetirada() {
        return dataHoraRetirada;
    }

    public DateTime getDataHoraDevolucao() {
        return dataHoraDevolucao;
    }

    public int getIdadeConcutor() {
        return idadeConcutor;
    }

    public long getNumCC() {
        return numCC;
    }

    public int getParcelas() {
        return parcelas;
    }

}

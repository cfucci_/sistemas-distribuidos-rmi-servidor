package model;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by Christian on 12/05/15.
 */
public class Promocao {
    private DateTime dataInicio;
    private DateTime dataFim;
    private double diaria;
    private Veiculo veiculo;

    public Promocao(DateTime dataInicio, DateTime dataFim, double diaria, Veiculo veiculo) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.diaria = diaria;
        this.veiculo = veiculo;
    }

    public DateTime getDataInicio() {
        return dataInicio;
    }

    public DateTime getDataFim() {
        return dataFim;
    }

    public double getDiaria() {
        return diaria;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }
}

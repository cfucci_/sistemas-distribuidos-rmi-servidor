package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Christian on 10/05/15.
 */
public class Veiculo implements Serializable{
    private String modelo;
    private int ano;
    private double diaria;
    private ArrayList<Locacao> locacoes = new ArrayList<Locacao>();

    public Veiculo(String modelo, int ano, double diaria) {
        this.modelo = modelo;
        this.ano = ano;
        this.diaria = diaria;
        this.locacoes = new ArrayList<Locacao>();
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public double getDiaria() {
        return diaria;
    }

    public void setDiaria(double diaria) {
        this.diaria = diaria;
    }

    public ArrayList<Locacao> getLocacoes() {
        return locacoes;
    }

    public void addLocacao(Locacao locacoes) {
        this.locacoes.add(locacoes);
    }
}

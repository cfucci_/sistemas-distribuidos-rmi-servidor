package controller;

import interfaces.InteresseInterface;
import model.Interesse;
import model.Lists;
import model.Veiculo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Christian on 24/05/15.
 */
public class InteresseControllerRemote extends UnicastRemoteObject implements InteresseInterface {

    public InteresseControllerRemote() throws RemoteException {

    }

    @Override
    public void createInteresse(Veiculo veiculo, String dataHoraRetirada, String dataHoraDevolucao) throws RemoteException{
        try{
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
            DateTime dataIni = formatter.parseDateTime(dataHoraRetirada);
            DateTime dataFim = formatter.parseDateTime(dataHoraDevolucao);
            String endCliente = getClientHost();
            Interesse interesse = new Interesse(veiculo, endCliente, dataIni, dataFim);
            Lists lists = Lists.getInstance();
            lists.getListInteresse().add(interesse);
        }catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
    }
}

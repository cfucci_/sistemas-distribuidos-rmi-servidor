package controller;


import interfaces.VeiculoInterface;
import model.Interesse;
import model.Lists;
import model.Locacao;
import model.Veiculo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Christian on 10/05/15.
 */
public class VeiculoControllerRemote extends UnicastRemoteObject implements VeiculoInterface {

    public VeiculoControllerRemote() throws RemoteException {
        super();
    }

    public ArrayList<Veiculo> pesquisaDisponibilidade(String dataR, String dataD) throws RemoteException{
        ArrayList<Veiculo> disponiveis = new ArrayList<Veiculo>();
        ArrayList<Veiculo> veiculos = Lists.getInstance().getListVeiculo();
        boolean disponivel = true;
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
        DateTime dataIni = formatter.parseDateTime(dataR);
        for (Veiculo v : veiculos){
            for (Locacao l : v.getLocacoes()){
                if(dataIni.isAfter(l.getDataHoraRetirada())){
                    disponivel = false;
                }
            }
            if (disponivel){
                disponiveis.add(v);
            }
        }
        return disponiveis;
    }

    public int registraInteresse(Veiculo veiculo, String dataHoraRetirada, String dataHoraDevolucao) throws RemoteException{
        DateTime dataIni = new DateTime(dataHoraRetirada);
        DateTime dataFim = new DateTime(dataHoraDevolucao);
        try {
            String address = getClientHost();
            Interesse interesse = new Interesse(veiculo, address, dataIni, dataFim);
            Lists.getInstance().getListInteresse().add(interesse);
            return 0;
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
            return 1;
        }
    }
    public int atualizaLocações(Veiculo veiculo) throws RemoteException{
        Lists lists = Lists.getInstance();
        ArrayList<Veiculo> veiculosList = lists.getListVeiculo();
        for(Veiculo v : veiculosList){
            if(v.getModelo().equals(veiculo.getModelo()) && v.getAno() == veiculo.getAno() && v.getDiaria() == veiculo.getDiaria()){
                veiculosList.remove(v);
                veiculosList.add(veiculo);
                break;
            }
        }
        return 0;
    }
}

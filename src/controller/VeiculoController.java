package controller;

import model.Lists;
import model.Veiculo;

/**
 * Created by Christian on 19/05/15.
 */
public class VeiculoController {

    public void createVeiculo(String modelo, int ano, double diaria){
        Veiculo veiculo = new Veiculo(modelo, ano, diaria);
        Lists lists = Lists.getInstance();
        lists.getListVeiculo().add(veiculo);
    }
}

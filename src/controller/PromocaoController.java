package controller;

import model.Interesse;
import model.Lists;
import model.Promocao;
import model.Veiculo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

/**
 * Created by Christian on 23/05/15.
 */
public class PromocaoController {
    private Veiculo veiculo;
    private DateTime dataIni;
    private DateTime dataFim;
    private double diaria;
    private Lists lists = Lists.getInstance();

    public PromocaoController(){

    }
    public int createPromocao(String modelo, String ano, String diaria, String dataIni, String dataFim){
        //Verifica a existência do veículo

        ArrayList<Veiculo> veiculos = lists.getListVeiculo();
        for(Veiculo v : veiculos){
            if(v.getModelo().equals(modelo) && Integer.toString(v.getAno()).equals(ano)){
                this.veiculo = v;
                break;
            }
        }
        if(veiculo == null){
            return 1;
        }
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
        this.dataIni = formatter.parseDateTime(dataIni);
        this.dataFim = formatter.parseDateTime(dataFim);
        if(this.dataIni.isAfter(this.dataFim)){
            return 2;
        }
        this.diaria = Double.parseDouble(diaria);
        Promocao promo = new Promocao(this.dataIni, this.dataFim, this.diaria, this.veiculo);
        lists.getListPromocoes().add(promo);
        verificaInteressados(this.veiculo, promo);
        return 0;
    }

    private void verificaInteressados(Veiculo veiculo, Promocao promocao) {
        ArrayList<Interesse> interesses = lists.getListInteresse();
        for(Interesse i : interesses){
            if(veiculo == i.getVeiculo() && promocao.getDataInicio().isAfter(i.getDataHoraRetirada()) && promocao.getDataFim().isAfter(i.getDataHoraDevolucao())){
                //dispara o método remoto para notificar o cliente!
                break;
            }
        }
    }
}

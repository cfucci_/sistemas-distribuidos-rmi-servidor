import controller.InteresseControllerRemote;
import controller.VeiculoControllerRemote;
import gui.TelaInicial;
import network.RmiController;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

/**
 * Created by Christian on 10/05/15.
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Hello World!");
        RmiController rmiController = RmiController.getInstance();
        try {
            VeiculoControllerRemote veiculoControllerRemote = new VeiculoControllerRemote();
            InteresseControllerRemote interesseControllerRemote = new InteresseControllerRemote();
            rmiController.createRemoteObjectVeiculo(veiculoControllerRemote);
            rmiController.createRemoteObjectInteresse(interesseControllerRemote);
        } catch (RemoteException e) {
            System.out.println(e.getMessage());
        }
        TelaInicial telaInicial = new TelaInicial();

    }
}

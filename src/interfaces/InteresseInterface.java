package interfaces;

import model.Veiculo;
        import org.joda.time.DateTime;

        import java.rmi.Remote;
        import java.rmi.RemoteException;

/**
 * Created by Christian on 24/05/15.
 */
public interface InteresseInterface extends Remote {
    public void createInteresse(Veiculo veiculo, String dataHoraRetirada, String dataHoraDevolucao) throws RemoteException;
}

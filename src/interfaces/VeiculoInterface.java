package interfaces;

import model.Veiculo;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by Christian on 10/05/15.
 */
public interface VeiculoInterface extends Remote {
    public ArrayList<Veiculo> pesquisaDisponibilidade(String dataR, String dataD) throws RemoteException;
    public int atualizaLocações(Veiculo veiculo) throws RemoteException;
}
